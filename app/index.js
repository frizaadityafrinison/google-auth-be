const express = require('express')
const router = require('./routes/route')
const cors = require('cors')
const app = express()

app.use(cors())
app.use(express.json())

module.exports = router.apply(app)