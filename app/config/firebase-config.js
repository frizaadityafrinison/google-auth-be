const admin = require('firebase-admin')
// const sequelize = require('firestore-sequelize')
const serviceAccount = require('./serviceAccount.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
})

// sequelize.initializeApp(admin)

module.exports = admin




