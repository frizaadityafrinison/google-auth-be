const admin = require("../config/firebase-config");
const { User } = require("../models");

module.exports = {
  handleIndex: (req, res) => {
    res.status(200).json({
      status: "OK",
      message: "IT WORKS!",
    });
  },
  createUser: async (req, res) => {
    try {
      const { provider, uid, name, email, password, alamat, kota, foto, nomorhp } = req.body;
      let userData;

      if (provider === "google.com") {
        const uidData = (await User.findOne({ where: { uid: uid } }))
          ? true
          : false;
        if (!uidData) {
          userData = await User.create({
            uid: uid,
            nama: name,
            alamat,
            kota,
            foto,
            nomorhp,
          });
        }
      } else {
        const data = await admin.auth().createUser({
          displayName: name,
          email,
          password,
        });
        userData = await User.create({
          uid: data.uid,
          nama: name,
          alamat,
          kota,
          foto,
          nomorhp,
        });
      }

      res.status(201).json(userData);
    } catch (err) {
      res.status(422).json({
        error: {
          name: err.message,
          message: err.message,
        },
      });
    }
  },
  infoAkun: async (req, res) => {
    try {
      const data = await User.findOne({
        where: {
          uid: req.user.uid,
        },
      });
      
      res.status(201).json(data);
    } catch (err) {
      res.status(422).json({
        error: {
          name: err.message,
          message: err.message,
        },
      });
    }
  },
  updateInfoAkun: async (req, res) => {
    const { nama, alamat, kota, foto, nomorhp } = req.body;
    try {
      await User.update(
        {
          nama,
          alamat,
          kota,
          foto,
          nomorhp,
        },
        {
          where: {
            uid: req.user.uid,
          },
        }
      );
    } catch (err) {
      res.status(422).json({
        error: {
          name: err.message,
          message: err.message,
        },
      });
    }
  },
};
