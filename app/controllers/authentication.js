const admin = require("../config/firebase-config");

module.exports = {
  authentication: async (req, res, next) => {
    try {
      const token = req.headers.authorization.split(" ")[1];
      const decodeValue = await admin.auth().verifyIdToken(token);
      
      if (decodeValue) {
        req.user = decodeValue
        next();
      }
    } catch (err) {
      res.status(401).json({
        error: {
          name: err.message,
          message: err.message,
          details: err.details || null,
        },
      });
    }
  },
};
