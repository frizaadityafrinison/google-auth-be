const Controller = require("../controllers/controller");
const Authentication = require('../controllers/authentication')

function apply(app) {
  app.route("/").get(Controller.handleIndex);
  app.route("/registrasi").post(Controller.createUser);
  app.route('/infoakun')
    .get(Authentication.authentication, Controller.infoAkun)
    .post(Authentication.authentication, Controller.updateInfoAkun)
  
  return app;
}

module.exports = { apply };
